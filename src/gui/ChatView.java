package gui;

import JmsComponents.Consumer;
import JmsComponents.Producer;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;


public class ChatView extends JPanel implements ActionListener {
    private JScrollPane scrollPane = new javax.swing.JScrollPane();
    private final boolean isGroupChat;
    private final JFileChooser chooser = new JFileChooser();
    private JTabbedPane jTabbedPane;
    private JPanel messagePanel = new JPanel();
    private Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
    private Consumer consumer;
    private JTextArea txtInput = new JTextArea();
    private Producer producer;
    private String chatname;

    ChatView(JTabbedPane jTabbedPane, boolean isGroupChat, Producer producer, Consumer consumer, String chatname) {
        this.jTabbedPane = jTabbedPane;
        this.isGroupChat = isGroupChat;
        this.producer = producer;
        this.consumer = consumer;
        this.chatname = chatname;
        if (isGroupChat) {
            consumer.receivefromTopic(chatname);
        }
        initComponents();
    }

    private void initComponents() {
        BoxLayout boxLayout = new BoxLayout(messagePanel, BoxLayout.Y_AXIS);
        messagePanel.setLayout(boxLayout);
        scrollPane.setViewportView(messagePanel);
        messagePanel.setSize(scrollPane.getSize());


        JButton addChat = new JButton("Neuer Einzelchat");
        addChat.setActionCommand("einzelchat");
        addChat.addActionListener(this);
        JButton addGroupChat = new JButton("Neuer Gruppenchat");
        addGroupChat.setActionCommand("gruppenchat");
        addGroupChat.addActionListener(this);

        JButton sendFile = new JButton("File senden");
        sendFile.setActionCommand("sendFile");
        sendFile.addActionListener(this);

        JButton sendText = new JButton("Senden");
        sendText.setActionCommand("send");
        sendText.addActionListener(this);


        txtInput.setToolTipText("");
        txtInput.setBorder(border);
        txtInput.setLineWrap(true);
        txtInput.setWrapStyleWord(true);


        JPanel buttonpanel = new JPanel();
        GridLayout gridLayout = new GridLayout(1, 4);
        buttonpanel.setLayout(gridLayout);
        buttonpanel.add(addChat);
        buttonpanel.add(addGroupChat);
        buttonpanel.add(sendFile);
        buttonpanel.add(sendText);

        BoxLayout boxLayout1 = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout1);
        this.add(scrollPane);
        this.add(txtInput);
        this.add(buttonpanel);


    }

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        String cmd = evt.getActionCommand();
        switch (cmd) {
            case "einzelchat":
                String name = JOptionPane.showInputDialog("Bitte geben sie den Namen des Einzelchats an!");
                if (name == null || name.equals("")) {
                    return;
                }
                jTabbedPane.addTab(name, new ChatView(jTabbedPane, false, producer, consumer, name));

                break;
            case "gruppenchat":
                name = JOptionPane.showInputDialog("Bitte geben sie den Namen des Gruppenchats an!");
                if (name == null || name.equals("")) {
                    return;
                }
                jTabbedPane.addTab(name, new ChatView(jTabbedPane, true, producer, consumer, name));
                break;
            case "sendFile":
                if (isGroupChat) {
                    JOptionPane.showMessageDialog(this, "SendFiles isn 't allowed for GroupChats!");
                } else {
                    int returnVal = chooser.showOpenDialog(this);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        String fname = file.getName();
                        String[] filenameAndExtension = fname.split("\\.");
                        String extension = filenameAndExtension[1];
                        if (extension.equals("jpg") || extension.equals("png")) {
                            updateTextAreaWithImage(file.getAbsolutePath(), true);
                        }
                        producer.sendFileMessageToQueue(file, chatname);
                    }
                }

                break;
            case "send":
                if (isGroupChat) {
                    producer.sendMessageToTopic(txtInput.getText(), chatname);
                } else {
                    producer.sendTextMessageToQueue(txtInput.getText(), chatname);
                }
                updateTextArea(txtInput.getText(), true, "");

                txtInput.setText("");
                break;
        }
    }

    public void updateTextArea(String message, boolean sentByMe, String senderName) {
        message = message.trim();
        if (!message.equals("")) {
            JPanel innerPanel = new JPanel();

            innerPanel.setLayout(new BorderLayout());
            JTextArea textArea;
            if (sentByMe || !isGroupChat) {
                textArea = new JTextArea(message);
            } else {
                textArea = new JTextArea(senderName + ":");
                textArea.append("\n");
                textArea.append(message);
            }
            textArea.setBorder(border);
            textArea.setEditable(false);
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            innerPanel.setSize(messagePanel.getSize());
            if (sentByMe) {
                innerPanel.add(textArea, BorderLayout.EAST);
            } else {
                innerPanel.add(textArea, BorderLayout.WEST);
            }
            messagePanel.add(innerPanel);
            messagePanel.add(Box.createRigidArea(new Dimension(0, 10)));
            messagePanel.revalidate();
            this.revalidate();
        }
    }

    public void updateTextAreaWithImage(String filepath, boolean sentByMe) {

        JPanel innerPanel = new JPanel();

        innerPanel.setLayout(new BorderLayout());

        JLabel picLabel;
        picLabel = new JLabel(new ImageIcon(filepath));
        innerPanel.setSize(messagePanel.getSize());
        if (sentByMe) {
            innerPanel.add(picLabel, BorderLayout.EAST);
        } else {
            innerPanel.add(picLabel, BorderLayout.WEST);
        }
        messagePanel.add(innerPanel);
        messagePanel.add(Box.createRigidArea(new Dimension(0, 10)));
        messagePanel.revalidate();
        this.revalidate();
    }
}
