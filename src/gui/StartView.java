package gui;

import JmsComponents.Consumer;
import JmsComponents.Producer;

import javax.swing.*;
import java.awt.*;

public class StartView extends JPanel {
    private JTabbedPane jTabbedPane;
    private Producer producer;
    private Consumer consumer;

    public StartView(JTabbedPane jTabbedPane, Producer producer, Consumer consumer) {
        this.jTabbedPane = jTabbedPane;
        this.producer = producer;
        this.consumer = consumer;
        BorderLayout borderLayout = new BorderLayout();
        this.setLayout(borderLayout);
        JButton b1 = new JButton("Neuer Gruppenchat");
        b1.setActionCommand("newgroupchat");
        b1.addActionListener(evt -> groupChatHandler());
        this.add(b1, BorderLayout.NORTH);
        JButton b2 = new JButton("Neuer Einzelchat");
        b2.setActionCommand("neweinzelchat");
        b2.addActionListener(evt -> einzelChatHandler());
        this.add(b2, BorderLayout.SOUTH);
    }

    private void groupChatHandler() {
        String input = JOptionPane.showInputDialog("Bitte geben sie den Namen des Gruppenchats an!");
        if (input == null || input.equals("")) {
            return;
        }

        jTabbedPane.removeTabAt(jTabbedPane.indexOfTab("Hallo"));
        jTabbedPane.addTab(input, new ChatView(jTabbedPane, true, producer, consumer, input));
    }

    private void einzelChatHandler() {
        String input = JOptionPane.showInputDialog("Bitte geben sie den Namen des Einzelchats an!");
        if (input == null || input.equals("")) {
            return;
        }
        jTabbedPane.removeTabAt(jTabbedPane.indexOfTab("Hallo"));
        jTabbedPane.addTab(input, new ChatView(jTabbedPane, false, producer, consumer, input));
    }

}
