package gui;

import JmsComponents.Client;
import JmsComponents.Consumer;
import JmsComponents.Producer;

import javax.swing.*;


public class MyFrame extends JFrame {

    private final JTabbedPane jTabbedPane = new JTabbedPane(JTabbedPane.LEFT, JTabbedPane.SCROLL_TAB_LAYOUT);
    private Producer producer;
    private Consumer consumer;

    public MyFrame() {
        producer = new Producer();
        consumer = new Consumer(this);
        consumer.receiveFromQueue();
        initComponents();
    }

    private void initComponents() {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException|ClassNotFoundException|InstantiationException|IllegalAccessException e) {
            e.printStackTrace();
        }

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Chat " + Client.USERNAME);

        this.add(jTabbedPane);

        jTabbedPane.addTab("Hallo", new StartView(jTabbedPane, producer, consumer));

        pack();
        this.setVisible(true);
    }


    public void updateTextArea(String message, boolean sentByMe, String senderName, String topic) {
        int tabid;
        if (!topic.isEmpty()) {
            tabid = jTabbedPane.indexOfTab(topic);
        } else {
            tabid = jTabbedPane.indexOfTab(senderName);
        }
        ChatView view;
        if (tabid == -1 && topic.isEmpty()) {
            view = new ChatView(jTabbedPane, false, producer, consumer, senderName);
            jTabbedPane.add(senderName, view);
        } else {
            view = (ChatView) jTabbedPane.getComponentAt(tabid);
        }
        view.updateTextArea(message, sentByMe, senderName);
    }

    public void updateTextAreaWithImage(String filepath, boolean sentByMe, String senderName) {
        int tabid = jTabbedPane.indexOfTab(senderName);
        ChatView view;
        view = (ChatView) jTabbedPane.getComponentAt(tabid);
        view.updateTextAreaWithImage(filepath, sentByMe);
    }
}





