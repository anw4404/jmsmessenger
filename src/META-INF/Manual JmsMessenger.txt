1. Install Docker
2. Switch to Folder with Dockerfile in it. Then run docker build -t activemq . (dont forget dot!!)
3. Build Container of Image with: docker run --name=<the name for container you want> -p <your port for messaging>:61616 -p <your port for Admin mask>:8161 activemq     Note: The Admin mask is reachable under this url: http://<hostaddress>:<your Admin mask Port>/admin
4. Add javax.jms Package to your Buildpath, which can be found in Jars folder                                                                                                                                Username: admin, Password:admin
5. Add external jar activemq-all-5.15.0.jar, which can be found in Jars folder
6. Build JmsMessenger.jar
7. Go to cmd and run java -jar JmsMessenger.jar tcp://<host adress of docker(you get it with docker-machine ip ,default is 192.168.99.100)>:<your port for messaging>
