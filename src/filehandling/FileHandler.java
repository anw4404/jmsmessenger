package filehandling;

import JmsComponents.Client;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class FileHandler {
    public FileHandler() {
    }

    public static byte[] convertTxtFileInByteArray(File file) {
        String data = "";
        try {
            data = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return data.getBytes();
    }

    public static byte[] convertImageFileInByteArray(File file, String extension) {
        ByteArrayOutputStream outputStream = null;
        try {
            BufferedImage image = ImageIO.read(file);
            outputStream = new ByteArrayOutputStream();
            ImageIO.write(image, extension, outputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    public static String InterpretByteArrayAsFile(byte[] bytearray, String extension, String filename) {
        String pathname = Client.SAVE_FILE_PATH + filename + "." + extension;
        File file = new File(pathname);
        try {
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bytearray);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (extension.equals("txt")) {
            return "text";
        } else {
            return file.getAbsolutePath();
        }
    }
}