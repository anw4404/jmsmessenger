package JmsComponents;

import filehandling.FileHandler;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.io.File;

public class Producer {
    private Connection connection;
    private Session session;
    private MessageProducer producer;

    public Producer() {
    }

    public void sendTextMessageToQueue(String text, String destinationQueue) {
        try {
            Destination destination = createJmsQueueSession(destinationQueue);
            TextMessage message = session.createTextMessage(text);
            message.setJMSDestination(destination);
            message.setJMSCorrelationID(Client.USERNAME);
            message.setStringProperty("topic", ""); //Set Topic Property empty to decide, if message was send from Topic or Queue
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void sendBytesMessageToQueue(byte[] messagebody, String extension, String filename, String destinationQueue) {
        try {
            Destination destination = createJmsQueueSession(destinationQueue);
            BytesMessage message = session.createBytesMessage();
            if (!extension.equals("")) {
                message.setStringProperty("extension", extension);
            }
            message.setStringProperty("filename", filename);
            message.writeBytes(messagebody);
            message.setJMSDestination(destination);
            message.setJMSCorrelationID(Client.USERNAME);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void sendFileMessageToQueue(File f, String destinationQueue) {
        String extension = "";
        String filename = "";
        byte[] messagebody = null;
        String fname = f.getName();
        String[] filenameAndExtension = fname.split("\\.");
        filename = filenameAndExtension[0];
        extension = filenameAndExtension[1];

        if (extension.equals("txt")) {
            messagebody = FileHandler.convertTxtFileInByteArray(f);
        } else if (extension.equals("jpg") || extension.equals("png")) {
            messagebody = FileHandler.convertImageFileInByteArray(f, extension);
        }
        sendBytesMessageToQueue(messagebody, extension, filename, destinationQueue);
    }

    private Destination createJmsQueueSession(String destinationQueue) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Client.HOSTNAME + "?wireFormat.maxFrameSize=1004857600");
        connection = null;
        Destination destination = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue(destinationQueue);
            producer = session.createProducer(destination);

        } catch (JMSException e) {
            e.printStackTrace();
        }
        return destination;
    }

    public void sendMessageToTopic(String text, String destinationTopic) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Client.HOSTNAME);
        connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createTopic(destinationTopic);
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);
            TextMessage message = session.createTextMessage(text);
            message.setJMSDestination(destination);
            message.setJMSCorrelationID(Client.USERNAME);
            message.setStringProperty("topic", destinationTopic);
            producer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}