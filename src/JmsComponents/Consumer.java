package JmsComponents;

import filehandling.FileHandler;
import gui.MyFrame;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class Consumer implements MessageListener {

    private Connection connection;
    MessageConsumer consumer;
    TopicSubscriber subscriber;
    private Session session;
    private MyFrame myFrame;

    public Consumer(MyFrame frame) {
        myFrame = frame;
    }

    public void receiveFromQueue() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Client.HOSTNAME);
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue(Client.USERNAME);
            consumer = session.createConsumer(destination);
            consumer.setMessageListener(this);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            String text = null;
            String correlationID = "";
            try {
                text = textMessage.getText();
                correlationID = textMessage.getJMSCorrelationID();
                if (correlationID.equals(Client.USERNAME)) {
                    text = null;
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
            if (text != null) {
                String topic = "";
                try {
                    topic = textMessage.getStringProperty("topic");

                } catch (JMSException e) {
                    e.printStackTrace();
                }
                myFrame.updateTextArea(text, false, correlationID, topic);
            }

        } else if (message instanceof BytesMessage) {
            BytesMessage bytesMessage = (BytesMessage) message;
            int arraylength = 0;
            try {
                arraylength = (int) bytesMessage.getBodyLength();
                byte[] bytearray = new byte[arraylength];
                bytesMessage.readBytes(bytearray);
                String correlationID = bytesMessage.getJMSCorrelationID();
                String filename = bytesMessage.getStringProperty("filename");
                String extension = bytesMessage.getStringProperty("extension");
                String newfilepath = FileHandler.InterpretByteArrayAsFile(bytearray, extension, filename);
                if (newfilepath.equals("text")) {
                    myFrame.updateTextArea("A File with name " + filename + "." + extension + " was send to you!", false, correlationID, "");
                } else {
                    myFrame.updateTextAreaWithImage(newfilepath, false, correlationID);
                }
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public void receivefromTopic(String destinationTopic) {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Client.HOSTNAME);
            connection = connectionFactory.createConnection();
            connection.setClientID("I am" + Client.USERNAME);
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic destination = session.createTopic(destinationTopic);
            subscriber = session.createDurableSubscriber(destination, Client.USERNAME);
            subscriber.setMessageListener(this);

        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}