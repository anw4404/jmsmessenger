package main;

import JmsComponents.Client;
import gui.MyFrame;

import javax.swing.*;

public class StartJmsApplication {

    public static void main(String[] args) {
        String hostname = args[0];
        String username = JOptionPane.showInputDialog("Bitte geben Sie Ihre Emailadresse ein!");
        if (username == null || username.equals("")) {
            System.exit(0);
        }
        new Client(username, hostname);
        new MyFrame();
    }
}
